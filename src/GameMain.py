#!/usr/bin/env python2.5
# -*- coding: utf-8 -*-
import random
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import mlab
from matplotlib import rcParams


# 初始参数设置
Box_sum = 500         # 箱子中剩余硬币数量，初始值
People_Flag = random.randint(1,10)       # flag 模拟人们取硬币或放硬币的概率 1~10 
Threshold = 2.5        #  阈值，可调： 1~Threshold 为取硬币，Threshold+1 ~10 为放硬币
Max_TakeCoin = 5       #  最多可取硬币数量
Max_DonateCoin = 1     #  最多可放硬币数量

delata = 0                # 取、放硬币数量
Box_per_remain = [500]   # 每次箱子中硬币余额，list


# 算法模拟
for x in range(1,5000):  # 循环次数表示参与人数
    flag = random.randint(1,10) # flag 模拟人们取硬币或放硬币的概率 
    if flag > Threshold:
    # 放硬币
        delta = random.randint(1,Max_DonateCoin)
        delta = random.randint(1,delta)          # 模拟了人们捐款可能性，有偏少的倾向
        Box_sum = Box_sum + delta
        Box_per_remain.append(Box_sum)
    else:
 	# 取硬币
        delta = random.randint(1,Max_TakeCoin)
        delta = random.randint(delta,Max_TakeCoin) # 模拟了人 取硬币的可能性，偏多的倾向
        if  Box_sum < delta:
            Box_sum = 0                           # 如果不够取，则取光
        else:
            Box_sum= Box_sum - delta
            Box_per_remain.append(Box_sum)

print(Box_per_remain)


# 绘图区
fig = plt.figure()

## 1. 标题、X、Y 轴 label
plt.title('Subway testing')
plt.xlabel('Time')
plt.ylabel('Money remained')

x= np.arange(len(Box_per_remain))

## 2. data

plt.plot(x,Box_per_remain,color='r')
plt.bar(x,Box_per_remain,alpha=.5,color='g')

plt.show()
